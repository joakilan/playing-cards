package no.jlangvand.idatt2001.cards.model;

import no.jlangvand.idatt2001.cards.utils.Rank;
import no.jlangvand.idatt2001.cards.utils.Suit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HandTest {
  Hand hand;
  static final PlayingCard aceOfSpades = new PlayingCard(Suit.SPADES, Rank.ACE);
  static final PlayingCard twoOfClubs = new PlayingCard(Suit.CLUBS, Rank.TWO);
  static final PlayingCard kingOfHearts = new PlayingCard(Suit.HEARTS,
      Rank.KING);

  @BeforeEach void setUp() {
    hand = new Hand();
    hand.add(twoOfClubs);
    hand.add(aceOfSpades);
  }

  @Test
  void numberOfPairs() {
    assertEquals(0, hand.nOfAKind(2));
    hand.add(aceOfSpades);
    assertEquals(1, hand.nOfAKind(2));
    hand.add(twoOfClubs);
    hand.add(kingOfHearts);
    assertEquals(2, hand.nOfAKind(2));
  }

  @Test
  void pair() {
    // Should be true when there's one and only one pair on the hand.
    assertFalse(hand.pair());
    hand.add(aceOfSpades);
    assertTrue(hand.pair());
    hand.add(twoOfClubs);
    assertFalse(hand.pair());
  }

  @Test
  void twoPairs() {
    // Should be tru when two pairs are the highest scoring combination.
    assertFalse(hand.twoPairs());
    hand.add(aceOfSpades);
    assertFalse(hand.twoPairs());
    hand.add(twoOfClubs);
    assertTrue(hand.twoPairs());
    hand.add(twoOfClubs);
    assertFalse(hand.twoPairs());
  }

  @Test
  void highCard() {
    assertEquals(aceOfSpades, hand.highCard(true));
    assertEquals(twoOfClubs, hand.highCard(false));
    hand.add(kingOfHearts);
    assertEquals(aceOfSpades, hand.highCard(true));
    assertEquals(kingOfHearts, hand.highCard(false));
  }

  @Test
  void threeOfAKind() {
    assertFalse(hand.threeOfAKind());
    hand.add(aceOfSpades);
    assertFalse(hand.threeOfAKind());
    hand.add(aceOfSpades);
    assertTrue(hand.threeOfAKind());
    hand.add(aceOfSpades);
    assertFalse(hand.threeOfAKind());
  }

  @Test
  void fourOfAKind() {
    assertFalse(hand.fourOfAKind());
    hand.add(aceOfSpades);
    assertFalse(hand.fourOfAKind());
    hand.add(aceOfSpades);
    assertFalse(hand.fourOfAKind());
    hand.add(aceOfSpades);
    assertTrue(hand.fourOfAKind());
  }

  @Test
  void straight() {
    assertFalse(hand.straight());
    hand.add(Suit.CLUBS, Rank.EIGHT);
    hand.add(Suit.DIAMONDS, Rank.NINE);
    hand.add(Suit.HEARTS, Rank.TEN);
    hand.add(Suit.SPADES, Rank.JACK);
    hand.add(Suit.CLUBS, Rank.JACK);
    assertFalse(hand.straight());
    hand.add(Suit.DIAMONDS, Rank.QUEEN);
    assertTrue(hand.straight());
  }

  @Test
  void flush() {
    assertFalse(hand.flush());
    for (int i = 0; i < 4; i++) hand.add(aceOfSpades);
    assertTrue(hand.flush());
  }

  @Test
  void fullHouse() {
    assertFalse(hand.fullHouse());
    hand.clear();
    hand.add(Suit.CLUBS, Rank.EIGHT);
    hand.add(Suit.DIAMONDS, Rank.EIGHT);
    hand.add(Suit.HEARTS, Rank.QUEEN);
    hand.add(Suit.SPADES, Rank.QUEEN);
    assertFalse(hand.fullHouse());
    hand.add(Suit.CLUBS, Rank.QUEEN);
    assertTrue(hand.fullHouse());

    // Make sure four of a kind is not treated as a full house
    hand.clear();
    Arrays.stream(Suit.values()).forEach(suit -> hand.add(suit, Rank.ACE));
    assertFalse(hand.fullHouse());
  }
}