package no.jlangvand.idatt2001.cards.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DeckOfCardsTest {
  DeckOfCards deck;

  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }

  @Test
  void sizeOfDeck() {
    assertEquals(52, deck.size());
  }
}