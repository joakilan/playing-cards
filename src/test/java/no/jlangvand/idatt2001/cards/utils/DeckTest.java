package no.jlangvand.idatt2001.cards.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DeckTest {
  Deck<Integer> deck;

  @BeforeEach void setUp() {
    deck = new Deck<>();
    deck.addToTop(0);
    deck.addToTop(1);
  }

  @Test
  void addToTop() {
    // There should be two cards added by setUp()
    assertEquals(2, deck.size());
    deck.addToTop(2);
    assertEquals(3, deck.size());
    assertEquals(2, deck.drawFromTop());
  }

  @Test
  void addToBack() {
    assertEquals(2, deck.size());
    deck.addToBack(2);
    assertEquals(3, deck.size());
    assertEquals(2, deck.drawFromBack());
  }

  @Test
  void drawFromTop() {
    deck.addToTop(2);
    deck.addToTop(3);
    // Making sure the size is as expected
    assertEquals(4, deck.size());
    // Cards are added to the top by default, drawFromTop() should therefore
    // return the topmost card
    assertEquals(3, deck.drawFromTop());
    // ..and remove it from the deck, decreasing the size
    assertEquals(3, deck.size());
  }

  @Test
  void drawFromBack() {
    deck.addToTop(2);
    deck.addToTop(3);
    assertEquals(4, deck.size());
    // The first card added by setUp() was 0
    assertEquals(0, deck.drawFromBack());
    assertEquals(3, deck.size());
  }
}