package no.jlangvand.idatt2001.cards.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RankTest {
  @Test
  void getValue() {
    assertEquals(1, Rank.ACE.getValue());
    assertEquals(14, Rank.ACE.getAlternateValue());
    assertEquals(2, Rank.TWO.getValue());
    assertEquals(2, Rank.TWO.getAlternateValue());
  }

  @Test
  void testToString() {
    assertEquals("A", Rank.ACE.toString());
    assertEquals("2", Rank.TWO.toString());
  }
}