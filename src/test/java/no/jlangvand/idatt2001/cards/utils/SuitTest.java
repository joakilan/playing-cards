package no.jlangvand.idatt2001.cards.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SuitTest {
  @Test
  void testToString() {
    assertEquals("♥", Suit.HEARTS.toString());
    assertEquals("♦", Suit.DIAMONDS.toString());
    assertEquals("♣", Suit.CLUBS.toString());
    assertEquals("♠", Suit.SPADES.toString());
  }
}