/*
 * Copyright (c) 2021 Joakim S. Langvand (@jlangvand).
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package no.jlangvand.idatt2001.cards.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Generic deck of objects.
 *
 * <p>Provides methods to add or pick objects from the front, the back, and a
 * random position in the deck. When picking a card, the card is removed from
 * the deck.
 *
 * @param <T> type of object stored in the deck
 */
public class Deck<T> {
  protected List<T> cards;
  private final Random rng;

  public Deck() {
    this.cards = new ArrayList<>();
    this.rng = new Random();
  }

  /**
   * Add an object (a "card") to the top of the deck.
   *
   * @param t object to be placed upon the deck
   */
  public void addToTop(T t) {
    cards.add(0, t);
  }

  public void addToTop(List<T> list) {
    for (T t : list) addToTop(t);
  }

  /**
   * Add an object (a "card") to the back of the deck.
   *
   * @param t object to be placed at the back of the deck
   */
  public void addToBack(T t) {
    cards.add(t);
  }

  public void addToBack(List<T> list) {
    for (T t : list) addToBack(t);
  }

  /**
   * Shuffle an object (a "card") into the deck.
   *
   * <p>The object will be placed in a random position in the deck.
   *
   * @param t object to be shuffled into the deck
   */
  public void addCardWithShuffle(T t) {
    cards.add(rng.nextInt(cards.size()), t);
  }

  public void addCardWithShuffle(List<T> list) {
    for (T t : list) addCardWithShuffle(t);
  }

    /**
     * Remove object ("card") from deck.
     *
     * @param t object to remove
     * @return true if removed, false if object was not present in the deck
     */
  public boolean removeCard(T t) {
    return cards.remove(t);
  }

  /**
   * Draw the topmost object ("card") from the deck.
   *
   * <p>The object will be removed from the deck.
   *
   * @return the topmost object from the deck
   */
  public T drawFromTop() {
    T t = cards.get(0);
    cards.remove(t);
    return t;
  }

  public List<T> drawFromTop(int n) {
    List<T> list = new ArrayList<>();
    for (int i = 0; i < n; i++) list.add(drawFromTop());
    return list;
  }

  /**
   * Draw the bottommost object "(card)" from the deck.
   *
   * <p>The object will be removed from the deck.
   *
   * @return the bottommost object from the deck
   */
  public T drawFromBack() {
    T t = cards.get(cards.size() - 1);
    cards.remove(t);
    return t;
  }

  public List<T> drawFromBack(int n) {
    List<T> list = new ArrayList<>();
    for (int i = 0; i < n; i++) list.add(drawFromBack());
    return list;
  }

  /**
   * Draw a random card from the deck.
   *
   * @return a random card from the deck
   */
  public T drawRandom() {
    T t = cards.get(rng.nextInt(cards.size()));
    cards.remove(t);
    return t;
  }

  public List<T> drawRandom(int n) {
    List<T> list = new ArrayList<>();
    for (int i = 0; i < n; i++) list.add(drawRandom());
    return list;
  }

  /**
   * Get number of cards in deck.
   *
   * @return number of cards in the deck
   */
  public int size() {
    return cards.size();
  }
}
