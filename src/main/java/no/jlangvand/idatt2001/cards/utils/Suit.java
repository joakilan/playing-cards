/*
 * Copyright (c) 2021 Joakim S. Langvand (@jlangvand).
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package no.jlangvand.idatt2001.cards.utils;

public enum Suit {
  CLUBS(0, "♣", "black"),
  DIAMONDS(1, "♦", "red"),
  HEARTS(2, "♥", "red"),
  SPADES(3, "♠", "black");

  private final int value;
  private final String symbol;
  private final String colour;

  Suit(int value, String symbol, String colour) {
    this.value = value;
    this.symbol = symbol;
    this.colour = colour;
  }

  public int toInt() {
    return value;
  }

  public String getColour() {
    return colour;
  }

  @Override
  public String toString() {
    return symbol;
  }
}
