/*
 * Copyright (c) 2021 Joakim S. Langvand (@jlangvand).
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package no.jlangvand.idatt2001.cards.model;

import no.jlangvand.idatt2001.cards.utils.Rank;
import no.jlangvand.idatt2001.cards.utils.Suit;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Deck of conventional playing cards.
 */
public class DeckOfCards extends ArrayDeque<PlayingCard> {
  Random rng;

  /**
   * Creates a new deck of 52 conventional playing cards.
   *
   * <p>The deck is initialised in series, Clubs from Ace to King, Diamonds
   * from Ace to King and so on.
   */
  public DeckOfCards() {
    rng = new Random();
    initialiseDeck();
  }

  /**
   * Add a card to the top of the deck.
   *
   * <p>Mainly a convenience method for designing tests.
   *
   * @param suit suit of card
   * @param rank rank of card
   */
  public void add(Suit suit, Rank rank) {
    super.addFirst(new PlayingCard(suit, rank));
  }

  /**
   * Draw n cards from the top of the deck.
   *
   * <p>Returns the rest of the deck if less than n cards remaining.
   *
   * @param n number of cards to draw
   * @return list of cards
   */
  public List<PlayingCard> draw(int n) {
    n = Math.min(n, size());
    return IntStream.range(0, n).mapToObj(i -> removeFirst())
        .collect(Collectors.toList());
  }

  /**
   * Shuffle the deck.
   *
   * <p>Can be called at any time to randomise the sequence of cards.
   */
  public void shuffle() {
    ArrayList<PlayingCard> cards = new ArrayList<>(this);
    clear();
    while (!cards.isEmpty()) add(cards.remove(rng.nextInt(cards.size())));
  }

  public int sum() {
    return stream().mapToInt(PlayingCard::value).sum();
  }

  public int alternateSum() {
    return stream().mapToInt(PlayingCard::alternateValue).sum();
  }

  private void initialiseDeck() {
    for (Suit suit : Suit.values()) {
      for (Rank rank : Rank.values()) {
        add(new PlayingCard(suit, rank));
      }
    }
  }
}
