package no.jlangvand.idatt2001.cards.model;

import no.jlangvand.idatt2001.cards.utils.Rank;
import no.jlangvand.idatt2001.cards.utils.Suit;

import java.util.Objects;

/**
 * Represents a playing card. A playing card has a Rank and a Suit.
 *
 * @author ntnu, jlangvand
 * @version 2021-03-04
 */
public class PlayingCard implements Comparable<PlayingCard> {
  private final Suit suit;
  private final Rank rank;

  /**
   * Creates an instance of a PlayingCard with a given Rank and Suit.
   *
   * @param suit The suit of the card, as a Suit enum
   * @param rank The rank of the card, as a Rank enum
   */
  public PlayingCard(Suit suit, Rank rank) {
    this.suit = suit;
    this.rank = rank;
  }

  /**
   * Returns the suit and face of the card as a string.
   *
   * <p>A 4 of hearts is returned as the string "♥4".
   *
   * @return the suit and face of the card as a string
   */
  public String getAsString() {
    return String.format("%s%s", suit, rank);
  }

  @Override
  public String toString() {
    return getAsString();
  }

  /**
   * Returns the suit of the card.
   *
   * @return the suit of the card as a Suit enum
   */
  public Suit getSuit() {
    return suit;
  }

  /**
   * Returns the rank of the card
   *
   * @return the face of the card as a Rank enum
   */
  public Rank getRank() {
    return rank;
  }

  /**
   * Get the numerical value of the card, where Ace is 1.
   *
   * @return the card's numerical value
   */
  public int value() {
    return rank.getValue();
  }

  /**
   * Get the numerical value of the card, where Ace is 14.
   *
   * @return the card's numerical value
   */
  public int alternateValue() {
    return rank.getAlternateValue();
  }

  @Override
  public int compareTo(PlayingCard playingCard) {
    int thisValue = this.getRank().getValue();
    int otherValue = playingCard.getRank().getValue();
    if (thisValue != otherValue) {
      return Integer.compare(thisValue, otherValue);
    }
    return this.getSuit().compareTo(playingCard.getSuit());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof PlayingCard)) return false;
    PlayingCard card = (PlayingCard) o;
    return getSuit() == card.getSuit() && getRank() == card.getRank();
  }

  @Override
  public int hashCode() {
    return Objects.hash(getSuit(), getRank());
  }
}