/*
 * Copyright (c) 2021 Joakim S. Langvand (@jlangvand).
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package no.jlangvand.idatt2001.cards.model;

import no.jlangvand.idatt2001.cards.utils.Rank;
import no.jlangvand.idatt2001.cards.utils.Suit;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Representing a hand of conventional playing cards.
 *
 * <p>Provides methods for checking the hand for common combinations (pairs,
 * flush...)
 */
public class Hand extends DeckOfCards {
  /**
   * Create an empty Hand.
   */
  public Hand() {
    super();
    clear();
  }

  /**
   * Check if the hand contains exactly one pair.
   *
   * <p>Returns false if the hand contains any combination of higher value.
   *
   * @return true if a pair is the highest value combination
   */
  public boolean pair() {
    return !fourOfAKind() && !threeOfAKind()
        && !twoPairs() && nOfAKind(2) > 0;
  }

  /**
   * Check if the hand contains two pairs.
   *
   * <p>Returns false if the hand contains any combination of higher value,
   * including four of a kind.
   *
   * @return true if two pairs is the highest value combination
   */
  public boolean twoPairs() {
    return !fourOfAKind() && !threeOfAKind() && nOfAKind(2) > 1;
  }

  /**
   * Check if the hand contains exactly three of a kind.
   *
   * <p>Returns false if the hand contains any combination of higher value.
   *
   * @return true if three of a kind is the highest value combination
   */
  public boolean threeOfAKind() {
    return !fourOfAKind() && nOfAKind(3) > 0;
  }

  /**
   * Check if the hand contains fr of a kind.
   *
   * @return true if a pair is the highest value combination
   */
  public boolean fourOfAKind() {
    return nOfAKind(4) > 0;
  }

  public int nOfAKind(int n) {
    return Arrays.stream(Rank.values())
        .mapToInt(r -> filterByRank(r).size() / n).sum();
  }

  public PlayingCard highCard(boolean aceIsHighest) {
    if (super.isEmpty()) {
      return null;
    }
    if (aceIsHighest) {
      Optional<PlayingCard> card =
          stream().filter(c -> c.getRank() == Rank.ACE).findFirst();
      if (card.isPresent()) {
        return card.get();
      }
    }
    return stream().max(Comparator.naturalOrder()).orElseThrow();
  }

  public boolean straight() {
    if (size() < 5) return false;
    int sequence = 0;
    for (Rank rank : Rank.values()) {
      if (stream().anyMatch(c -> c.getRank().equals(rank))) sequence++;
      else sequence = 0;
      if (sequence == 5) return true;
    }
    return false;
  }

  public boolean flush() {
    if (size() < 5) return false;
    return Arrays.stream(Suit.values())
        .anyMatch(suit -> stream().filter(c -> c.getSuit().equals(suit))
            .count() >= 5);
  }

  public boolean fullHouse() {
    return size() == 5
        && nOfAKind(4) <= 0
        && nOfAKind(3) + nOfAKind(2) == 3;
  }

  public List<PlayingCard> filterBySuit(Suit suit) {
    return stream()
        .filter(c -> c.getSuit().equals(suit)).collect(Collectors.toList());
  }

  public List<PlayingCard> filterByRank(Rank rank) {
    return stream()
        .filter(c -> c.getRank().equals(rank)).collect(Collectors.toList());
  }

  @Override
  public String toString() {
    if (isEmpty()) return "Empty";
    StringBuilder str = new StringBuilder();
    for (PlayingCard card : this) {
      str.append(card.toString());
    }
    return str.toString().strip();
  }
}