/*
 * Copyright (c) 2021 Joakim S. Langvand (@jlangvand).
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package no.jlangvand.idatt2001.cards.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import no.jlangvand.idatt2001.cards.model.DeckOfCards;
import no.jlangvand.idatt2001.cards.model.Hand;
import no.jlangvand.idatt2001.cards.model.PlayingCard;
import no.jlangvand.idatt2001.cards.utils.Rank;
import no.jlangvand.idatt2001.cards.utils.Suit;

public class MainController {
  private DeckOfCards deck;
  private Hand hand;

  @FXML
  void initialize() {
    this.deck = new DeckOfCards();
    this.hand = new Hand();
    updateLabelsAndControls();
  }

  @FXML
  private Label sumLabel;

  @FXML
  private Label cardsInDeckLabel;

  @FXML
  private Label pairsLabel;

  @FXML
  private Label tripleLabel;

  @FXML
  private Label highLabel;

  @FXML
  private Label straightLabel;

  @FXML
  private Label flushLabel;

  @FXML
  private Label houseLabel;

  @FXML
  private AnchorPane rootPane;

  @FXML
  private JFXButton drawButton;

  @FXML
  private JFXButton shuffleButton;

  @FXML
  private JFXButton resetDeckButton;

  @FXML
  private JFXListView<PlayingCard> deckList;

  @FXML
  private JFXListView<PlayingCard> handList;

  @FXML
  private JFXListView<PlayingCard> heartsList;

  @FXML
  private Label whereIsTheQueen;

  @FXML
  void drawHand() {
    hand = new Hand();
    hand.addAll(deck.draw(5));
    updateLabelsAndControls();
  }

  @FXML
  void throwHand() {
    hand = new Hand();
    updateLabelsAndControls();
  }

  @FXML
  void resetDeck() {
    throwHand();
    deck = new DeckOfCards();
    updateLabelsAndControls();
  }

  @FXML
  void shuffleDeck() {
    deck.shuffle();
    updateLabelsAndControls();
  }

  private void updateLabelsAndControls() {
    sumLabel.setText(hand.sum() + "/" + hand.alternateSum());
    pairsLabel.setText("" + hand.nOfAKind(2));
    highLabel.setText(
        hand.isEmpty() ? "N/A" : hand.highCard(true).toString());
    highLabel.setStyle(
        hand.isEmpty() ? "" : String.format("-fx-text-fill: %s",
        hand.highCard(true).getSuit().getColour()));
    tripleLabel.setText("" + hand.nOfAKind(3));
    cardsInDeckLabel.setText("" + deck.size());
    booleanLabel(straightLabel, hand.straight());
    booleanLabel(flushLabel, hand.flush());
    booleanLabel(houseLabel, hand.fullHouse());
    drawButton.setDisable(deck.size() < 5);
    resetDeckButton.setDisable(deck.size() == 52);
    deckList.setItems(FXCollections.observableArrayList(deck));
    handList.setItems(FXCollections.observableArrayList(hand));
    heartsList.setItems(FXCollections.observableArrayList(
        hand.filterBySuit(Suit.HEARTS)));
    String theQueen;
    if (deck.stream().anyMatch(c -> c.getSuit().equals(Suit.SPADES)
        && c.getRank().equals(Rank.QUEEN))) theQueen = "is in the deck";
    else if (hand.stream().anyMatch(c -> c.getSuit().equals(Suit.SPADES)
        && c.getRank().equals(Rank.QUEEN))) theQueen = "is on the hand";
    else theQueen = "is gone";
    whereIsTheQueen.setText(theQueen);
  }

  private void booleanLabel(Label label, boolean bool) {
    label.setText(bool ? "Yes" : "No");
    label.setStyle(
        String.format("-fx-text-fill: %s", bool ? "green" : "black"));
  }
}
