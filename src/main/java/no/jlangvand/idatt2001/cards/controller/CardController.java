/*
 * Copyright (c) 2021 Joakim S. Langvand (@jlangvand).
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package no.jlangvand.idatt2001.cards.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import no.jlangvand.idatt2001.cards.model.PlayingCard;
import no.jlangvand.idatt2001.cards.utils.Rank;
import no.jlangvand.idatt2001.cards.utils.Suit;

public class CardController {
  @FXML
  private Label topSuit;

  @FXML
  private Label topRank;

  @FXML
  private Label centerSuit;

  @FXML
  private Label bottomSuit;

  @FXML
  private Label bottomRank;

  public void createCard(PlayingCard card) {
    Suit suit = card.getSuit();
    Rank rank = card.getRank();
    String style = String.format("-fx-text-fill: %s", suit.getColour());
    topSuit.setText(suit.toString());
    topSuit.setStyle(style);
    centerSuit.setText(suit.toString());
    centerSuit.setStyle(style);
    bottomSuit.setText(suit.toString());
    bottomSuit.setStyle(style);
    topRank.setText(rank.toString());
    topRank.setStyle(style);
    bottomRank.setText(rank.toString());
    bottomRank.setStyle(style);
  }

  public void showBackside() {
    String style = "-fx-text-fill: #aaa";
    topSuit.setText("");
    topSuit.setStyle("");
    centerSuit.setText("?");
    centerSuit.setStyle(style);
    bottomSuit.setText("");
    bottomSuit.setStyle(style);
    topRank.setText("");
    topRank.setStyle(style);
    bottomRank.setText("");
    bottomRank.setStyle(style);
  }
}
