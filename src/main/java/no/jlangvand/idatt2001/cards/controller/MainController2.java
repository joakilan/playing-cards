/*
 * Copyright (c) 2021 Joakim S. Langvand (@jlangvand).
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package no.jlangvand.idatt2001.cards.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import no.jlangvand.idatt2001.cards.model.DeckOfCards;
import no.jlangvand.idatt2001.cards.model.Hand;
import no.jlangvand.idatt2001.cards.model.PlayingCard;
import no.jlangvand.idatt2001.cards.utils.Rank;
import no.jlangvand.idatt2001.cards.utils.Suit;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainController2 {
  @FXML
  private BorderPane cardPane0;

  @FXML
  private BorderPane cardPane1;

  @FXML
  private BorderPane cardPane2;

  @FXML
  private BorderPane cardPane3;

  @FXML
  private BorderPane cardPane4;

  @FXML
  private JFXListView<String> infobox;

  @FXML
  private JFXButton drawFiveButton;

  @FXML
  private JFXButton shuffleButton;

  @FXML
  private JFXButton resetButton;

  private Hand hand;
  private DeckOfCards deck;
  private CardController[] cardControllers;
  private final BorderPane[] cardPanes = new BorderPane[5];

  @FXML
  private void initialize() throws IOException {
    hand = new Hand();
    deck = new DeckOfCards();
    cardControllers = new CardController[5];
    cardPanes[0] = cardPane0;
    cardPanes[1] = cardPane1;
    cardPanes[2] = cardPane2;
    cardPanes[3] = cardPane3;
    cardPanes[4] = cardPane4;
    for (int i = 0; i < 5; i++) {
      FXMLLoader loader = getCardLoader();
      Parent pane = loader.load();
      cardControllers[i] = loader.getController();
      cardControllers[i].showBackside();
      cardPanes[i].setCenter(pane);
    }
    updateInfoAndControls();
  }

  @FXML
  public void drawFiveAction() {
    hand.clear();
    hand.addAll(deck.draw(5));
    updateInfoAndControls();
    renderCards();
  }

  @FXML
  public void shuffleAction() {
    deck.shuffle();
  }

  @FXML
  public void resetAction() {
    hand.clear();
    deck = new DeckOfCards();
    updateInfoAndControls();
    for (int i = 0; i < 5; i++) cardControllers[i].showBackside();
  }

  private void renderCards() {
    ArrayList<PlayingCard> copyOfHand = new ArrayList<>(hand);
    for (int i = 0; i < 5; i++) {
      cardControllers[i].createCard(copyOfHand.get(i));
    }
  }

  private FXMLLoader getCardLoader() throws MalformedURLException {
    URL url = new File("src/main/resources/views/Card.fxml")
        .toURI().toURL();
    FXMLLoader cardLoader = new FXMLLoader();
    cardLoader.setLocation(url);
    return cardLoader;
  }

  private void updateInfoAndControls() {
    ArrayList<String> info = new ArrayList<>();

    info.add("Cards in deck: " + deck.size());

    info.add("Number of pairs: " + hand.nOfAKind(2));
    info.add("Three of a kind: " + (hand.nOfAKind(3) > 0 ? "Yes" : "No"));
    info.add("Four of a kind: " + (hand.nOfAKind(4) > 0 ? "Yes" : "No"));

    info.add("Highest card (Ace is 1): " + (hand.isEmpty() ? "N/A" :
        hand.highCard(false)));
    info.add("Highest card (Ace is 14): " + (hand.isEmpty() ? "N/A" :
        hand.highCard(true)));

    info.add("Straight: " + (hand.straight() ? "Yes" : "No"));
    info.add("Flush: " + (hand.flush() ? "Yes" : "No"));
    info.add("Full house: " + (hand.fullHouse() ? "Yes" : "No"));

    StringBuilder sb = new StringBuilder("Hearts on hand: ");
    ArrayList<PlayingCard> hearts =
        new ArrayList<>(hand.filterBySuit(Suit.HEARTS));
    if (hearts.isEmpty()) sb.append("None");
    else hearts.forEach(c -> sb.append(c.toString()).append(" "));
    info.add(sb.toString().strip());

    String theQueen =
        (new PlayingCard(Suit.SPADES, Rank.QUEEN)).toString() + " is ";
    if (deck.stream().anyMatch(c -> c.getSuit().equals(Suit.SPADES)
        && c.getRank().equals(Rank.QUEEN))) theQueen += "in the deck";
    else if (hand.stream().anyMatch(c -> c.getSuit().equals(Suit.SPADES)
        && c.getRank().equals(Rank.QUEEN))) theQueen += "on the hand";
    else theQueen += "gone";
    info.add(theQueen);

    info.add("Sum of values (Ace is 1): " + hand.sum());
    info.add("Sum of values (Ace is 14): " + hand.alternateSum());

    infobox.setItems(FXCollections.observableArrayList(info));

    drawFiveButton.setDisable(deck.size() < 5);
  }
}
