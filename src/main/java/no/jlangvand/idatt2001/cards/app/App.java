package no.jlangvand.idatt2001.cards.app;

import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class App extends Application {

  @Override
  public void start(Stage stage) throws IOException {
    URL url = new File("src/main/resources/views/Main2.fxml").toURI().toURL();
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(url);
    Parent root = loader.load();
    JFXDecorator decorator = new JFXDecorator(stage, root);
    decorator.setCustomMaximize(true);
    decorator.setTitle("Oblig 3");
    Scene scene = new Scene(decorator, 800, 600);
    stage.setScene(scene);
    stage.setTitle("Oblig 3");
    stage.show();
  }

  public static void main(String[] args) {
    launch();
  }

}