module cards.app {
  requires javafx.graphics;
  requires javafx.controls;
  requires javafx.fxml;
  requires com.jfoenix;

  exports no.jlangvand.idatt2001.cards.app;
  exports no.jlangvand.idatt2001.cards.model;
  exports no.jlangvand.idatt2001.cards.utils;

  opens no.jlangvand.idatt2001.cards.controller to javafx.fxml;
  exports no.jlangvand.idatt2001.cards.controller;
}